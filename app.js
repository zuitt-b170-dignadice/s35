const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// access to routes

const userRoutes = require("./routes/userRoutes")
const courseRoutes = require("./routes/courseRoutes")

// server
const app = express();
const port = 4000

app.use(cors()) // allows all origins/domains to access the backend application

app.use(express.json())
app.use(express.urlencoded({extended:true}))


app.use("/api/users", userRoutes)
app.use("/api/courses", courseRoutes)

mongoose.connect("mongodb+srv://admin:admin@cluster0.h8y6g.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
    useNewUrlParser : true,
    useUnifiedTopology : true
})

let db = mongoose.connection;
db.on("error", console.error.bind(console, "Connection Error"));
db.once("open", () => console.log("we're connected to the database"))

app.listen(port, () => console.log(`API now online at port ${port}`))

//   app.listen(process.env.port, () => console.log(`API now online at port ${port}`)

// activty s34    

/* 
Activity:
1. Refactor the course route to implement user authentication for the admin when creating a course.
2. Refactor the addCourse controller method to implement admin authentication for creating a course.
3. Create a git repository named S34.
4. Add another remote link and push to git with the commit message of Add activity code.
5. Add the link in Boodle.
*/
